export default
{
	name : "Fonctions",
	methods :
	{
		rechercheDeLivres
	}
}

export function rechercheDeLivres(titre, livres)
{
	let resultat = [];

	for (let i = 0; i < livres.length; ++i)
	{
		if (livres[i].indexOf(titre) != -1)
		{
			resultat.push(livres[i]);
		}
	}

	return resultat;
}
